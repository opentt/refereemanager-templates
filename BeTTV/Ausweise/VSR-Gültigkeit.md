title:	Gültigkeit Verbandsschiedsrichter
subtitle:	BeTTV
options:	noemail, nosponsorlogo
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	BeTTV_VSR-Gueltigkeit.md
outputpath: ../../Dokumente/Ausweise

```{=latex}
\footnotesize

\begin{longtable}[l]{@{}p{.34\textwidth}@{\hspace{.01\textwidth}}p{.12\textwidth}@{\hspace{.06\textwidth}}p{.34\textwidth}@{\hspace{.01\textwidth}}p{.12\textwidth}@{}}
		\toprule
```
`{\scriptsize`{=latex}**Name**`} & {\scriptsize`{=latex}**Lizenz bis**`} & {\scriptsize`{=latex}**Name**`} & {\scriptsize`{=latex}**Lizenz bis**`}\\`{=latex}
```{=latex}
		\midrule
	\endhead
		\midrule
```
`\multicolumn{4}{r}{{\scriptsize`{=latex}*weiter auf der nächsten Seite...*`}}\\`{=latex}
```{=latex}
		\bottomrule
	\endfoot
		\bottomrule
	\endlastfoot
```

<#--
	code from ../refs_bettv.ftl
	maybe sometime I can include the code but not yet
-->
<#assign association_bettv = "Verband.BeTTV" />
<#assign refs_bettv = [] />
<#list selection?filter(it -> (it.association??)) as referee>
	<#list referee.association as association>
		<#if (association.association.id == association_bettv) >
			<#assign refs_bettv = refs_bettv + [referee] />
		</#if>
	</#list>
</#list>
<#-- /code from ../refs_bettv.ftl -->

<#list refs_bettv?filter(it -> it.active) as referee>
<@compress single_line=true>
<#if referee?index % 2 == 0 ><#if referee?index % 4 == 0 >`\rowcolor{Linen}`{=latex}<#else>`\rowcolor{white}`{=latex}</#if></#if>
${referee.tableName.value} `&`{=latex}
<#if referee.nextTrainingUpdate??>${referee.nextTrainingUpdate.value?date["yyyy-MM-dd"]?string["31.12.yyyy"]}<#else>---</#if>
<#if referee?item_parity == "odd" >`&`{=latex}<#else>`\\`{=latex}</#if>
<#if (!referee?has_next) && (referee?item_parity == "odd") >`&`{=latex}</#if>
</@compress>

</#list>

```{=latex}
\end{longtable}
```
