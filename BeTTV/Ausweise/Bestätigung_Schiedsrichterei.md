title:	Bestätigung der Schiedsrichter<#if (current.sexType.id == 'SexType.female') >innen</#if>-Tätigkeit
opening: Sehr geehrte Damen und Herren,
closing: Mit freundlichen Grüßen,
signature: Ekkart Kleinod.
options: nosponsorlogo
filename:	Bestätigung_SR_${current.fileName.value}_${.now?date?string["yyyy-MM-dd"]}.md
outputpath: ../Bestätigungen/

<#setting locale="de">

<#if current.sexType.id == 'SexType.female' >Frau<#else>Herr</#if> ${current.displayTitle.value}

<#if current.birthday?? >
geboren am: ${current.birthday.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}
</#if>

<#if current.primaryAddress??>
wohnhaft

<#if current.primaryAddress.street??>${current.primaryAddress.street.value}</#if> <#if current.primaryAddress.number??>${current.primaryAddress.number.value}</#if>\
<#if current.primaryAddress.zipCode??>${current.primaryAddress.zipCode.value}</#if> <#if current.primaryAddress.city??>${current.primaryAddress.city.value}</#if>
</#if>

ist

<#assign hasSince=false />
<#list current.trainingLevel as traininglevel>
- ${traininglevel.type.displayText.value} für den <#if (traininglevel.type.id == 'TrainingLevelType.VSR') >Berliner Tisch-Tennis Verband<#else>Deutschen Tischtennis-Bund</#if> <#if traininglevel.since?? >seit ${traininglevel.since.value?date['yyyy-MM-dd']?string['yyyy']}<#assign hasSince=true /></#if>
</#list>

und <#if (hasSince) >seitdem</#if> aktiv
in <#if (current.sexType.id == 'SexType.female') >ihrem<#else>seinem</#if> Verein
<#if current.reffor??>${current.reffor.displayText.value}<#else><#if current.member?? >${current.member.displayText.value}<#else>---</#if></#if>
für den Verband als Schiedsrichter<#if (current.sexType.id == 'SexType.female') >in</#if> tätig.
