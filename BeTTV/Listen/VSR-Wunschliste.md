title:	VSR-Tagung -- Wünsche, Vorlieben
subtitle:
options:	noemail, nosponsorlogo
date:			29. April 2024
filename:	08_Wunschliste.md
outputpath: ../../VSR-Tagung/${refereeManagerData.data.season.title.value}

Ihr könnt **Vereine**, **Ligen**, oder **Wochentage** angeben oder ob Ihr eher **Ligaspiele** oder **Turniere** bevorzugt.

```{=latex}
\footnotesize

\begin{longtable}[l]{@{}>{\RaggedRight}p{.25\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.43\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.30\textwidth}@{}}
		\toprule
```
`{\scriptsize`{=latex}**Name**`} & {\scriptsize`{=latex}**Bevorzugt schiedsen**`} & {\scriptsize`{=latex}**Nicht schiedsen**`}\\`{=latex}
```{=latex}
		\midrule
	\endhead
		\midrule
```
`\multicolumn{3}{@{}r@{}}{{\scriptsize`{=latex}*weiter auf der nächsten Seite...*`}}\\`{=latex}
```{=latex}
		\bottomrule
	\endfoot
		\bottomrule
	\endlastfoot
```

<#list selection as referee>
<@compress single_line=true>
${referee.tableName.value}
`&`{=latex}
<#list referee.prefer as prefer>${prefer.displayText.value}<#sep>`\newline `{=latex}</#sep></#list>
`&`{=latex}
<#list referee.avoid as avoid>${avoid.displayText.value}<#sep>`\newline `{=latex}</#sep></#list>
`\\`{=latex}
</@compress>

<#sep>`\midrule`{=latex}</#sep>
</#list>

```{=latex}
\end{longtable}
```
