title:	Kontaktliste für den VSRA
subtitle:	BTTV, Saison ${refereeManagerData.data.season.title.value}
options:	noemail
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	${refereeManagerData.data.season.title.value}_BeTTV_VSR-Kontaktliste-VSRA.md
outputpath: ../Listen

```{=latex}
\footnotesize



\begin{longtable}[l]{@{}>{\RaggedRight}p{.25\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.41\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.24\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.07\textwidth}@{}}
		\toprule
```
`{\scriptsize`{=latex}**Name**`} & {\scriptsize`{=latex}**Kontakt**`} & {\scriptsize`{=latex}**Adresse**`} & {\scriptsize`{=latex}**Ausb.**`}\\`{=latex}
```{=latex}
		\midrule
	\endhead
		\midrule
```
`\multicolumn{4}{@{}r@{}}{{\scriptsize`{=latex}*weiter auf der nächsten Seite...*`}}\\`{=latex}
```{=latex}
		\bottomrule
	\endfoot
		\bottomrule
	\endlastfoot
```

<#--
	code from ../refs_bettv.ftl
	maybe sometime I can include the code but not yet
-->
<#assign association_bettv = "Verband.BeTTV" />
<#assign refs_bettv = [] />
<#list selection?filter(it -> (it.association??)) as referee>
	<#list referee.association as association>
		<#if (association.association.id == association_bettv) >
			<#assign refs_bettv += [referee] />
		</#if>
	</#list>
</#list>
<#-- /code from ../refs_bettv.ftl -->

<#assign used_status_types = [] />

<#list refs_bettv as referee>
<@compress single_line=true>

<#if referee?item_parity == "odd" >`\rowcolor{Linen}`{=latex}<#else>`\rowcolor{white}`{=latex}</#if>

<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.tableName.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>
`&`{=latex}
<#list referee.phoneNumber as phonenumber><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${phonenumber.displayText.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if><#sep> `\newline`{=latex} </#sep></#list><#if referee.phoneNumber?? && (referee.phoneNumber?size > 0) && referee.EMail?? && (referee.EMail?size > 0) > `\mbox{}\newline`{=latex} </#if><#list referee.EMail as email><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${email.displayText.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if><#sep> `\newline`{=latex} </#sep></#list>
`&`{=latex}
<#if referee.primaryAddress??><#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.primaryAddress.displayText.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if></#if>
`&`{=latex}
<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.highestTrainingLevel.type.shorttitle.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>
`\\`{=latex}

<#if referee.remark?? && referee.remark.value != "" >
<#if referee?item_parity == "odd" >`\rowcolor{Linen}`{=latex}<#else>`\rowcolor{white}`{=latex}</#if>
`& \multicolumn{3}{@{}>{\RaggedRight}p{.74\textwidth}@{}}{`{=latex}<#if referee.status.mmdmarkupstart??>${referee.status.mmdmarkupstart.value}</#if>${referee.remark.value}<#if referee.status.mmdmarkupend??>${referee.status.mmdmarkupend.value}</#if>`}\\`{=latex}
</#if>

<#if !(used_status_types?seq_contains(referee.status)) >
	<#assign used_status_types += [referee.status] />
</#if>

</@compress>

</#list>

```{=latex}
\end{longtable}
```

**Legende:**

<#list used_status_types as statustype>
- <#if statustype.mmdmarkupstart??>${statustype.mmdmarkupstart.value}</#if><#if statustype.remark??>${statustype.remark.value}<#else>${statustype.title.value}</#if><#if statustype.mmdmarkupend??>${statustype.mmdmarkupend.value}</#if>
</#list>
