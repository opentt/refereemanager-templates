<#assign show_inactive = false >
<#assign show_canceled = false >
filename:	events.md
outputpath: ../Nachrichten

<#assign event_array = [] />
<#list selection as event>
	<#if event.dayPeriod gt 1>
		<#assign lastday = event.lastDay >
	<#else>
		<#assign lastday = event.firstDay >
	</#if>
	<#assign active = ((.now?date lte lastday.date.value?date.iso) && (!event.canceled)) >
	<#if (!event.canceled || show_canceled) >
		<#if (active || show_inactive) >
			<#assign event_array = event_array + [event] />
		</#if>
	</#if>
</#list>

<#list event_array as event>
<#list event.day as day>
<@compress single_line=true>
- ${day.date.value?date.iso?string["EE dd.MM.yyyy"]},
<#if day.startTime?? > ${day.startTime.value?time.iso?string["HH:mm"]}<#if day.endTime?? > – ${day.endTime.value?time.iso?string["HH:mm"]}</#if> Uhr</#if>
${event.displayTitleShort.value}
<#if event.remark?? > (${event.remark.value})</#if>
</@compress>
</#list>

<#else>
Alle Turniere besetzt.
</#list>
