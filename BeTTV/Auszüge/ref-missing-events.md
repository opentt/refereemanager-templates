<#assign show_inactive = false >
<#assign show_canceled = false >
filename:	ref-missing-events.md
outputpath: ../Nachrichten

<#assign event_array = [] />
<#list selection as event>
	<#if event.dayPeriod gt 1>
		<#assign lastday = event.lastDay >
	<#else>
		<#assign lastday = event.firstDay >
	</#if>
	<#assign active = ((.now?date lte lastday.date.value?date.iso) && (!event.canceled)) >
	<#if (!event.canceled || show_canceled) >
		<#if (active || show_inactive) >
			<#assign event_array = event_array + [event] />
		</#if>
	</#if>
</#list>

# OSR/SRaT Turniere

<#list event_array as event>
<@compress single_line=true>
- ${event.dateRangeText.value}, ${event.displayTitleShort.value}:
<#assign further = false />
<#assign missingCountSum = 0 />
<#list refereeManagerData.data.refereeAssignmentType?filter(it -> (event.getMissingCountFor(it) gt 0)) as type>
	${event.getMissingCountFor(type)} ${type.displayTitleShort.value}<#sep>, </#sep>
	<#assign missingCountSum = missingCountSum + event.getMissingCountFor(type) />
</#list>
<#if (missingCountSum == 0) >
	vollständig besetzt
<#else>
	gesucht
</#if>
</@compress>

<#else>
Keine Turniere.
</#list>
