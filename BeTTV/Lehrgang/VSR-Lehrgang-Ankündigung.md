title: Schirilehrgang Saison ${refereeManagerData.data.season.title.value}
date: <#setting locale="de"> ${.now?string["MMMM yyyy"]}
options: pagestyle=leer, nosponsorlogo
filename:	VSR-Lehrgang-Ankündigung.md
outputpath: ../Lehrgang

<#setting locale="de">
Die Berliner Schiedsrichterinnen und Schiedsrichter suchen Verstärkung.

Voraussetzung ist, dass Du mindestens 14 Jahre und Mitglied eines Berliner Tischtennisvereins bist.

Was erwartet Dich?

- Einsätze als Oberschiedsrichterin bzw. Oberschiedsrichter von Bundesliga bis Oberliga
- Einsätze als Schiedsrichterin bzw. Schiedsrichter bei Turnieren in Berlin
- bei Interesse Weiterbildung für nationale und internationale Einsätze

Der nächste Verbandsschiedsrichterlehrgang findet an zwei Wochenenden im Oktober ${.now?string["yyyy"]} statt. Folgende Termine sind wichtig:

<#list refereeManagerData.data.otherEvent as event>
<#if (event.type.id == "EventDateType.VSRTraining") >
- ${event.firstDay.date.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}: ${event.displayTitleShort.value?replace("VSR-Lehrgang - ", "")} (${event.firstDay.date.value?date['yyyy-MM-dd']?string['EEEE']})<#if event.firstDay.startTime??>, ${event.firstDay.startTime.value}<#if event.firstDay.endTime??> -- ${event.firstDay.endTime.value}</#if> Uhr</#if>
</#if>
</#list>

Die Anmeldung erfolgt beim Verbandsschiedsrichterausschuss (VSRA)

> <vsrausschuss@bettv.de>

Die Kosten für den Lehrgang betragen 15 Euro pro Person und werden traditionell vom Verein übernommen.

Mehr Details findest Du auf den Schiriwebseiten unter

> <https://www.tt-schiri.de/schiri-werden.html>
