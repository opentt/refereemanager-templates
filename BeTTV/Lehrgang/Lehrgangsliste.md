title:	Lehrgangsliste Verbandsschiedsrichter
subtitle:	BTTV, Saison ${refereeManagerData.data.season.title.value}
options:	noemail
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	${refereeManagerData.data.season.title.value}_BeTTV_VSR-Lehrgangsliste.md
outputpath: ../Lehrgang

<#list refereeManagerData.data.statusType?filter(status -> (status.id?contains("trainee"))) as statustype>

# ${statustype.displayTitleShort.value}

<#list selection?filter(ref -> (ref.status?? && (statustype.id == ref.status.id)))>

```{=latex}
\footnotesize
\begin{longtable}[l]{@{}>{\RaggedRight}p{.05\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.25\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.43\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.24\textwidth}@{}}
		\toprule
```
`{\scriptsize`{=latex}**#**`} & {\scriptsize`{=latex}**Name**`} & {\scriptsize`{=latex}**Kontakt**`} & {\scriptsize`{=latex}**Mitglied**`\newline `{=latex}***Schiedst für***`}\\`{=latex}
```{=latex}
		\midrule
	\endhead
		\midrule
```
`\multicolumn{4}{@{}r@{}}{{\scriptsize`{=latex}*weiter auf der nächsten Seite...*`}}\\`{=latex}
```{=latex}
		\bottomrule
	\endfoot
		\bottomrule
	\endlastfoot
```

<#items as referee>
<@compress single_line=true>
<#if referee?item_parity == "odd" >`\rowcolor{Linen}`{=latex}<#else>`\rowcolor{white}`{=latex}</#if>
${referee?counter}
`&`{=latex}
${referee.tableName.value}
`&`{=latex}
<#list referee.phoneNumber?filter(contact -> !contact.editorOnly) as phonenumber>${phonenumber.displayText.value}<#sep>`\newline `{=latex}</#sep></#list><#if referee.phoneNumber?? && (referee.phoneNumber?size > 0) && referee.EMail?? && (referee.EMail?size > 0) >`\mbox{}\newline `{=latex}</#if><#list referee.EMail?filter(contact -> !contact.editorOnly) as email>${email.displayText.value}<#sep>`\newline `{=latex}</#sep></#list>
`&`{=latex}
<#if referee.member??>${referee.member.displayTitleShort.value}</#if><#if referee.reffor??>`\newline `{=latex}*${referee.reffor.displayTitleShort.value}*</#if>
`\\`{=latex}

<#if referee.remark?? && referee.remark.value != "" >
<#if referee?item_parity == "odd" >`\rowcolor{Linen}`{=latex}<#else>`\rowcolor{white}`{=latex}</#if>
`& \multicolumn{3}{@{}>{\RaggedRight}p{.74\textwidth}@{}}{`{=latex}*${referee.remark.value}*`}\\`{=latex}
</#if>

</@compress>

</#items>

```{=latex}
\end{longtable}
```

</#list>

</#list>
