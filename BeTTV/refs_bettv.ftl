<#assign association_bettv = "Verband.BeTTV" />
<#assign refs_bettv = [] />
<#list selection?filter(it -> (it.association??)) as referee>
	<#list referee.association as association>
		<#if (association.association.id == association_bettv) >
			<#assign refs_bettv = refs_bettv + [referee] />
		</#if>
	</#list>
</#list>
