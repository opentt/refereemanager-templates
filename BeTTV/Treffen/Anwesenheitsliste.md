<#assign maxcount = 23 >
<#assign thedate = "2024-04-22"?date["yyyy-MM-dd"] >
title:	VSR-Fortbildung -- Anwesenheitsliste
filename:	${thedate?string["yyyy-MM-dd"]}_Anwesenheitsliste.md
outputpath: ../../${refereeManagerData.data.season.title.value}/Fortbildungen/
<#--
title:	VSR-Tagung -- Anwesenheitsliste
filename:	09_Anwesenheitsliste.md
outputpath: ../../VSR-Tagung/${refereeManagerData.data.season.title.value}
-->
subtitle:
options:	noemail, nosponsorlogo
date:			${thedate?string["d. MMMM yyyy"]}

```{=latex}
\begin{longtable}[l]{@{}>{\RaggedRight}p{.05\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.3\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.3\textwidth}@{\hspace{.01\textwidth}}>{\RaggedRight}p{.32\textwidth}@{}}
		\toprule
		\textbf{\small Nr.} & \textbf{\small Name} & \textbf{\small Verein} & \textbf{\small Unterschrift}\\
		\midrule
	\endhead
		\midrule
		\multicolumn{4}{@{}r@{}}{\emph{\footnotesize weiter auf der nächsten Seite\dots}}\\
		\bottomrule
	\endfoot
		\bottomrule
	\endlastfoot
```

<#list selection as referee>
${referee?counter} `&`{=latex} ${referee.tableName.value} `&`{=latex} <#if referee.member??>${referee.member.displayText.value}</#if><#if referee.reffor??> *${referee.reffor.displayText.value}*</#if> `& \\`{=latex}
<#sep>`\midrule`{=latex}</#sep>
</#list>

<#if (selection?size + 1) &lt; maxcount>
	<#list (selection?size + 1)..maxcount as count>
`\midrule`{=latex}
${count} `&&& \\`{=latex}
	</#list>
</#if>

```{=latex}
\end{longtable}
```
