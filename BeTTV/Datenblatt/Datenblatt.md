title:	Datenblatt
subtitle:	${current.displayTitle.value}
date:			Stand: <#setting locale="de"> ${refereeManagerData.info.modified?datetime.iso?string["d. MMMM yyyy"]}
filename:	${current.fileName.value}.md
options:	notitlepage, noauthor, noemail, nosponsorlogo, pagestyle=leer
outputpath: Datenblätter/${.now?date?string['yyyy-MM-dd']}/

<#macro highlight doHighlight text remark=false>
<@compress single_line=true>
	<#if (remark) >
		<#if (doHighlight) >`\textcolor{DarkGray}{`{=latex} *${text}*`}`{=latex}</#if>
	<#else>
		<#if (doHighlight) >
			`\textcolor{OrangeRed}{\faIcon{hand-point-right}~`{=latex}${text}`}`{=latex}
		<#else>
			${text}
		</#if>
	</#if>
</@compress>
</#macro>

<#setting locale="de">

```{=latex}
	{
		\footnotesize
		\sffamily

		\newcolumntype{E}{@{}>{\scriptsize\bfseries}p{.23\textwidth}@{\hspace{.02\textwidth}}p{.75\textwidth}@{}}
		\begin{longtable}[l]{E}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für Click-TT, Vereine, DTTB, andere VSR, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

Name `&`{=latex} ${current.displayTitle.value}`\\`{=latex}
Ausbildung `&`{=latex} ${current.highestTrainingLevel.type.shorttitle.value} (${current.highestTrainingLevel.type.title.value})`\\`{=latex}
Mitglied bei `&`{=latex} <#if current.member?? >${current.member.displayText.value}<#else>---</#if>`\\`{=latex}
<#if current.reffor??>Schiedst für `&`{=latex} ${current.reffor.displayText.value}`\\`{=latex}</#if>
Status (kurz) `&`{=latex} <#if current.status.active.value >aktiv<#else>inaktiv</#if>`\\`{=latex}

```{=latex}
			\bottomrule
		\end{longtable}
```

```{=latex}
		\begin{longtable}[l]{E}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für Vereine, DTTB, andere VSR, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

<@highlight doHighlight=(current.EMail?size == 0) text="E-Mail" /> `&`{=latex} <#list current.EMail as email>${email.displayText.value}<#sep>; </#sep></#list><@highlight doHighlight=(current.EMail?size == 0) text="Hast Du evtl. eine E-Mail-Adresse für mich?" remark=true />`\\`{=latex}
Telefon `&`{=latex} <#list current.phoneNumber as phonenumber>${phonenumber.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
Geschlecht `&`{=latex} ${current.sexType.displayText.value}`\\`{=latex}

```{=latex}
			\bottomrule
		\end{longtable}
```

```{=latex}
		\begin{longtable}[l]{E}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für DTTB, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

<@highlight doHighlight=(!(current.birthday??)) text="Geburtstag" /> `&`{=latex} <#if current.birthday?? >${current.birthday.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}</#if><@highlight doHighlight=(!(current.birthday??)) text="Wäre schön, wenn wir den für die Statistik hätten." remark=true /> `\\`{=latex}

```{=latex}
			\bottomrule
		\end{longtable}
```

```{=latex}
		\begin{longtable}[l]{E}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für andere VSR, VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

Status `&`{=latex} ${current.status.title.value}`\\`{=latex}

<#if (current.status.id != "StatusType.mailonly") >
<#assign overdue = (!current.nextTrainingUpdate??) || (current.nextTrainingUpdate?? && (current.nextTrainingUpdate.value?date['yyyy-MM-dd']?string['yyyy']?number <= .now?date?string['yyyy']?number)) >
<@highlight doHighlight=overdue text="Nächste Fortbildung" /> `&`{=latex} <#if current.nextTrainingUpdate?? >${current.nextTrainingUpdate.value?date['yyyy-MM-dd']?string['yyyy']}</#if>`\\`{=latex}
</#if>

```{=latex}
			\bottomrule
		\end{longtable}
```

```{=latex}
		\begin{longtable}[l]{E}
```
`\multicolumn{2}{@{}l@{}}{`{=latex}**Sichtbar für VSRA**`}\\`{=latex}
```{=latex}
			\midrule
```

VSR-Mitteilungen `&`{=latex} <#if current.EMail?? && (current.EMail?size > 0) >per E-Mail<#if current.docsByLetterCombined.value> und </#if></#if><#if current.docsByLetterCombined.value>per Brief</#if>`\\`{=latex}
<@highlight doHighlight=(current.address?size == 0) text="Adresse" /> `&`{=latex} <#list current.address as address>${address.displayText.value}<#sep>; </#sep></#list><@highlight doHighlight=(current.address?size == 0) text="Für die örtliche Einsatzplanung wäre eine Adresse sehr hilfreich." remark=true />`\\`{=latex}
URL `&`{=latex} <#list current.URL as url>${url.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
Bevorzugt schiedsen `&`{=latex} <#list current.prefer as prefer>${prefer.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
Nicht schiedsen `&`{=latex} <#list current.avoid as avoid>${avoid.displayText.value}<#sep>; </#sep><#else>---</#list>`\\`{=latex}
<#list current.trainingLevel as traininglevel>
<@highlight doHighlight=(!(traininglevel.since??)) text=traininglevel.type.displayText.value + " seit" /> `&`{=latex} <#if traininglevel.since?? >${traininglevel.since.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}</#if><@highlight doHighlight=(!(traininglevel.since??)) text="Wäre schön, wenn wir das für die Statistik hätten." remark=true />`\\`{=latex}
</#list>
Letzte Fortbildung `&`{=latex} <#if current.lastTrainingUpdate?? >${current.lastTrainingUpdate.value?date['yyyy-MM-dd']?string['dd.MM.yyyy']}</#if>`\\`{=latex}
<#if current.remark??>Anmerkung `&`{=latex} ${current.remark.value}`\\`{=latex}</#if>

```{=latex}
			\bottomrule
		\end{longtable}
	}
```
