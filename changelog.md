# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- letter tournament participation
- scriptlets
- events
- several csv files
- all mails as text file

### Changed

- updates all files to new API
- moved from github to gitlab
- id cards, validity
- data-sheet
- updated files to Multimarkdown 6

### Deprecated

### Removed

### Fixed

### Security


## [0.3.0] - 2017-07-16

### Added

- data sheet
- letters for data sheets, license update, passive licenses

### Fixed

- wishes: "displayText" instead of "dislpayTitle"
- data-sheet: "displayText" instead of "displayTtitle"
- ISO date locale: de


## [0.2.0] - 2017-04-23

### Added

- BeTTV-Templates for id cards, lists, and meetings


## [0.1.0] - 2017-04-22

### Added

- basic templates for documents, emails, letters, and texts
