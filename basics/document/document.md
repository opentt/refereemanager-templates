Quotes Language:		german
Base Header Level:	3
latex leader:				kleinod-mmd-scrartcl
MyOptions:					author=schiri, font=droid, notitlepage, noauthor, pagestyle=fuss <#if filledInputData.getValueFor("options")??>, ${filledInputData.getValueFor("options")!}</#if>
Title:							${filledInputData.getValueFor("title")}
MySubTitle:					${filledInputData.getValueFor("subtitle")!}
Date:								${filledInputData.getValueFor("date")}
latex header:				\input{kleinod-mmd-style}
										\usepackage{tabularray}
latex begin:				kleinod-mmd-begin-doc

`\maketitle`{=latex}

${filledInputData.getValueFor("body")}
